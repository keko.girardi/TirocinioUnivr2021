﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Animation.Enemy.Brugon {

    public class BrugonEnemyAnimationController : CharacterAnimator {

        protected override void Start() {
            base.Start();
        }

        protected override void OnEnemyMove() {
            base.OnEnemyMove();
        }

        protected override void OnAttack() {
            base.OnAttack();
        }

    }

}
