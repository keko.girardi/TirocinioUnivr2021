﻿using Stat;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

namespace Enemy {

    public class EnemyController : MonoBehaviour {

        /// <summary>
        /// Enemy view distance
        /// </summary>
        [Header("Enemy properties")]
        [Tooltip("Enemy view distance")]
        public float lookRadius = 10;

        [HideInInspector]
        public bool canDoDamage;

        public event System.Action onEnemyMove;

        private CharacterCombat enemyCombat;

        private Transform target;

        private NavMeshAgent agent;

        private void Start() {
            StartCoroutine(TargetPlayer());
            agent = GetComponent<NavMeshAgent>();
            enemyCombat = GetComponent<CharacterCombat>();
        }

        private void Update() {
            float distance = float.MaxValue;

            if (target != null)
                distance = Vector3.Distance(target.position, transform.position);

            if (distance <= lookRadius) {
                agent.SetDestination(target.position);

                if (onEnemyMove != null)
                    onEnemyMove();

                FaceTarget();

                if (distance <= agent.stoppingDistance) {
                    AttackPlayer();
                    canDoDamage = true;
                } else
                    canDoDamage = false;
            }
        }

        private void OnDrawGizmos() {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, lookRadius);
        }

        private void AttackPlayer() {
            CharacterStats playerStats = target.GetComponent<CharacterStats>();
            if (playerStats != null)
                enemyCombat.Attack(playerStats);

            PlayerLogic.Killer = gameObject;
        }

        private void FaceTarget() {
            Vector3 direction = (target.position - transform.position).normalized;

            Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
        }

        private IEnumerator TargetPlayer() {
            yield return new WaitForSeconds(1);

            target = PlayerManager.Instance.playerObject.transform;
        }

    }

}
