﻿using UnityEngine;

namespace CustomEditorAttribute {

    /// <summary>
    /// Converts a string property into a Scene property in the inspector
    /// </summary>
    public class SceneAttribute : PropertyAttribute { }

}
